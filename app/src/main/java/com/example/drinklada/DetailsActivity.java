package com.example.drinklada;


import static com.example.drinklada.MainActivity.db;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.drinklada.adapters.ProductListAdapter;
import com.example.drinklada.models.CartDrink;
import com.example.drinklada.models.Drink;
import com.squareup.picasso.Picasso;

import java.util.List;

public class DetailsActivity extends AppCompatActivity {
//    int id = Integer.parseInt(getIntent().getStringExtra("id"));
    int item_quantity = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        Drink drink=(Drink) getIntent().getSerializableExtra("drinks");
        ImageView imageView= (ImageView) findViewById(R.id.details_image);
        TextView textView_name = (TextView) findViewById(R.id.details_drink_name);
        TextView textView_price =(TextView) findViewById(R.id.details_drink_price);
        TextView textView_description = (TextView) findViewById(R.id.details_drink_description);
        TextView details_add = (TextView) findViewById(R.id.details_add);
        TextView details_remove = (TextView) findViewById(R.id.details_remove);
        TextView details_item_quantity = (TextView) findViewById(R.id.details_item_quantity);
        Button check_out =(Button) findViewById(R.id.checkout_btn);
        RecyclerView similarItemRecycler =(RecyclerView) findViewById(R.id.similar_item_scroll);
        if (drink.getImage()!=null)
        Picasso.get().load(drink.getImage()).into(imageView);
        textView_name.setText(drink.getName());
        textView_price.setText(drink.getPrice());
        textView_description.setText(drink.getDescription());
        details_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                item_quantity = item_quantity+1;
                details_item_quantity.setText(String.valueOf(item_quantity));
            }
        });
        details_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (item_quantity>0) {
                    item_quantity = item_quantity - 1;
                    details_item_quantity.setText(String.valueOf(item_quantity));
                }
            }
        });
        check_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                db.insert(new CartDrink(drink.getImage(),drink.getName(),drink.getPrice(),(item_quantity>0?item_quantity:1)));
                Intent intent = new Intent(view.getContext(), CartActivity.class);
                startActivity(intent);
            }
        });
        db.getAllDrinks().observe(this, new Observer<List<Drink>>() {
            @Override
            public void onChanged(List<Drink> drinks) {
                ProductListAdapter adapter = new ProductListAdapter(drinks);
                similarItemRecycler.setHasFixedSize(true);
                similarItemRecycler.setLayoutManager(new LinearLayoutManager(
                        DetailsActivity.this, LinearLayoutManager.HORIZONTAL, false));
                similarItemRecycler.setAdapter(adapter);
            }
        });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_item, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.cart_item_id:
                Intent intent = new Intent(this.getApplicationContext(), CartActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}