package com.example.drinklada;

import static android.content.ContentValues.TAG;
import static com.example.drinklada.MainActivity.db;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.drinklada.models.Drink;

public class AdminActivity extends AppCompatActivity {
    EditText image_edt,name_edt,price_edt,description_edt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
         image_edt = (EditText) findViewById(R.id.image_edt);
         name_edt = (EditText)findViewById(R.id.name_edt);
         price_edt = (EditText)findViewById(R.id.price_edt);
         description_edt = (EditText)findViewById(R.id.description_edt);
        TextView error_edt = findViewById(R.id.error_edt);
        Button submit = findViewById(R.id.submit_btn);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String image = image_edt.getText().toString();
                final String name = name_edt.getText().toString();
                final String price = price_edt.getText().toString();
                final String description = description_edt.getText().toString();
                if(name.isEmpty()||image.isEmpty()||price.isEmpty()||description.isEmpty()){
                    Log.e(TAG, name+image+price+description);
                    error_edt.setVisibility(View.VISIBLE);
                    error_edt.setFocusable(true);
                    error_edt.setText("Fields can not be empty");
                }else {
                if (image.contains("https://")) {
                    db.insert(new Drink(image, name, "$"+price, 1, description));
                    name_edt.setText("");
                    price_edt.setText("");
                    description_edt.setText("");
                    image_edt.setText("");
                    Intent intent = new Intent(AdminActivity.this,ProductActivity.class);
                    startActivity(intent);
                }else {
                    error_edt.setVisibility(View.VISIBLE);
                    error_edt.setFocusable(true);
                    error_edt.setText("Incorrect image Link");
                }
                }
            }
        });
    }
}