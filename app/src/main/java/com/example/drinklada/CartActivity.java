package com.example.drinklada;

import static com.example.drinklada.MainActivity.db;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.drinklada.adapters.CartListAdapter;
import com.example.drinklada.models.CartDrink;

import java.util.List;

public class CartActivity extends AppCompatActivity {
    public static double total=0;
    static TextView total_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        Button cart_checkout = findViewById(R.id.cart_checkout);

        db.getAllCartDrinks().observe(this, new Observer<List<CartDrink>>() {
            @Override
            public void onChanged(List<CartDrink> cartDrinks) {
                for (int i = 0; i < cartDrinks.size(); i++) {
                    total = total + Double.parseDouble(cartDrinks.get(i).getPrice().substring(1));
                }
                RecyclerView recyclerView = (RecyclerView) findViewById(R.id.cart_recycler);
                total_view = findViewById(R.id.total_view);
                total_view.setText(String.valueOf(total));
                CartListAdapter adapter = new CartListAdapter(cartDrinks);
                recyclerView.setLayoutManager(new LinearLayoutManager(
                        CartActivity.this, LinearLayoutManager.VERTICAL, false));
                recyclerView.setAdapter(adapter);
            }
        });

        cart_checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CartActivity.this,CheckoutActivity.class);
                startActivity(intent);
            }
        });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_item, menu);
        return true;
    }
    public static void updateTotal(boolean add,double value){
        if (add){
            total+=value;
        }else {
            total -= value;
        }
        total_view.setText(String.valueOf(total));
    }
}