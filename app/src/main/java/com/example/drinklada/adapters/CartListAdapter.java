package com.example.drinklada.adapters;

import static com.example.drinklada.CartActivity.total;
import static com.example.drinklada.CartActivity.updateTotal;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.drinklada.CartActivity;
import com.example.drinklada.DetailsActivity;
import com.example.drinklada.R;
import com.example.drinklada.models.CartDrink;
import com.example.drinklada.models.Drink;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CartListAdapter extends RecyclerView.Adapter<CartListAdapter.ViewHolder>{
    private  List<CartDrink> listdata;

    // RecyclerView recyclerView;
    public CartListAdapter(List<CartDrink> listdata) {
        this.listdata = listdata;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.activity_cart_item, parent, false);
        return new ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final CartDrink myListData = listdata.get(position);

        holder.textView_name.setText(myListData.getName());
        holder.textView_price.setText(myListData.getPrice());
        holder.cart_item_quantity.setText(String.valueOf(myListData.getQuantity()));
        Picasso.get().load(myListData.getImage()).into(holder.imageView);
        holder.cart_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int item_quantity = myListData.getQuantity();
                myListData.setQuantity(item_quantity+1);
                holder.cart_item_quantity.setText(String.valueOf(myListData.getQuantity()));
                updateTotal(true,Double.parseDouble(myListData.getPrice().substring(1)));
            }
        });
        holder.cart_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int item_quantity = myListData.getQuantity();
                if (item_quantity>0)
                    myListData.setQuantity(item_quantity-1);
                holder.cart_item_quantity.setText(String.valueOf(myListData.getQuantity()));
                updateTotal(false,Double.parseDouble(myListData.getPrice().substring(1)));
            }
        });

    }


    @Override
    public int getItemCount() {
        return listdata.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        public TextView textView_name;
        public TextView textView_price;
        TextView cart_add,cart_remove,cart_item_quantity,total_view;
        public ViewHolder(View itemView) {
            super(itemView);
            this.imageView = (ImageView) itemView.findViewById(R.id.cart_imageView);
            this.textView_name = (TextView) itemView.findViewById(R.id.cart_drink_name);
            this.textView_price = (TextView) itemView.findViewById(R.id.cart_drink_price);
             cart_add = (TextView) itemView.findViewById(R.id.cart_add);
             cart_remove = (TextView) itemView.findViewById(R.id.cart_remove);
            cart_item_quantity = (TextView) itemView.findViewById(R.id.cart_item_quantity);
            total_view = (TextView) itemView.findViewById(R.id.total_view);
        }
    }

}
