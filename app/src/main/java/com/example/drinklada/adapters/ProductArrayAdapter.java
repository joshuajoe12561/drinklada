package com.example.drinklada.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;

import com.example.drinklada.DetailsActivity;
import com.example.drinklada.R;
import com.example.drinklada.models.CartDrink;
import com.example.drinklada.models.Drink;
import com.example.drinklada.models.ViewModal;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ProductArrayAdapter extends ArrayAdapter<Drink> {
    ViewModal db;
    public ProductArrayAdapter(@NonNull Context context, List<Drink> drinksModelArrayList, ViewModal db) {
        super(context, 0, drinksModelArrayList);
        this.db = db;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View listitemView = convertView;
        if (listitemView == null) {
            // Layout Inflater inflates each item to be displayed in GridView.
            listitemView = LayoutInflater.from(getContext()).inflate(R.layout.activity_drink_item, parent, false);
        }

        Drink drink = getItem(position);
         ImageView imageView= (ImageView) listitemView.findViewById(R.id.imageView);
         TextView textView_name = (TextView) listitemView.findViewById(R.id.drink_name);
         TextView textView_price =(TextView) listitemView.findViewById(R.id.drink_price);
        Button add_to_cart =(Button) listitemView.findViewById(R.id.add_to_cart_btn);
        CardView drinkItem = listitemView.findViewById(R.id.drink_item_layout);
        textView_name.setText(drink.getName());
        textView_price.setText(drink.getPrice());
        if (!drink.getImage().isEmpty())
        Picasso.get().load(drink.getImage()).into(imageView);
        drinkItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(view.getContext(),"click on item: "+drink.getName(),Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getContext(), DetailsActivity.class);
                intent.putExtra("drinks",drink);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                view.getContext().startActivity(intent);
            }
        });
        add_to_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                db.insert(new CartDrink(drink.getImage(),drink.getName(),drink.getPrice(),1));
            }
        });
        return listitemView;
    }
}
