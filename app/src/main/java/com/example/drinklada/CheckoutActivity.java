package com.example.drinklada;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class CheckoutActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        EditText name_edt = findViewById(R.id.editTextPersonName);
        EditText phone_edt = findViewById(R.id.editTextPhone);
        EditText address_edt = findViewById(R.id.editTextPostalAddress);
        LinearLayout success = findViewById(R.id.success_view);
        LinearLayout linearLayout = findViewById(R.id.checkout_form);
        Button chechout = findViewById(R.id.cart_checkout_btn);
        String name = name_edt.getText().toString();
        String phone = phone_edt.getText().toString();
        String address = address_edt.getText().toString();
        chechout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                success.setVisibility(View.VISIBLE);
                linearLayout.setVisibility(View.GONE);
            }
        });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_item, menu);
        return true;
    }
}