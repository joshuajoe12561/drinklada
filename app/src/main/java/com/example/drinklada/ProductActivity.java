package com.example.drinklada;

import static com.example.drinklada.MainActivity.db;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.GridView;

import com.example.drinklada.adapters.ProductArrayAdapter;
import com.example.drinklada.models.Drink;
import com.nex3z.notificationbadge.NotificationBadge;

import java.util.List;

public class ProductActivity extends AppCompatActivity {

    String[] images = new String[]{"https://images.pexels.com/photos/7333076/pexels-photo-7333076.jpeg?auto=compress&cs=tinysrgb&w=800",
            "https://images.pexels.com/photos/3721969/pexels-photo-3721969.jpeg?auto=compress&cs=tinysrgb&w=800",
            "https://images.pexels.com/photos/11675005/pexels-photo-11675005.png?auto=compress&cs=tinysrgb&w=800",
            "https://images.pexels.com/photos/4109193/pexels-photo-4109193.jpeg?auto=compress&cs=tinysrgb&w=800",
            "https://media.istockphoto.com/id/1211524213/photo/cup-of-coffee-latte-isolated-on-white-background-with-clipping-path.jpg?b=1&s=612x612&w=0&k=20&c=wyu7g0080fc_Rn0EXfxEd4eZ-o8y1YHrUsVRbo0zStk=",
            "https://media.istockphoto.com/id/155392568/photo/bottle-of-beer-with-custom-label-and-clipping-path.jpg?s=612x612&w=0&k=20&c=iIU1a9_58HvRTC-50sHh-Bo02KUuFTdJPXg7zMUhKwA=",
            "https://images.pexels.com/photos/6412586/pexels-photo-6412586.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2",
            "https://images.pexels.com/photos/11675006/pexels-photo-11675006.png?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2"};
    String CloveTeaDesc = "This special tea can also help with chest congestion and can help with relief from the sinus. The presence of eugenol in the spice can clear the congestion and provide relief. Cloves contain vitamin E and vitamin K which can help in fighting bacterial infection. This tea is also effective in treating fever";
    String cocktail = "A cocktail is an alcoholic mixed drink. Most commonly, cocktails are either a combination of spirits, or one or more spirits mixed with other ingredients such as tonic water, fruit juice, flavored syrup, or cream.";
    String Masi = "An impressive, complex nose of berry fruits and a touch of vanilla. Rich, baked fruit flavours on the palate with hints of cocoa. Impressive structure and length, with a soft and velvety finish.";
    String cocacola = "An impressive, complex nose of berry fruits and a touch of vanilla. Rich, baked fruit flavours on the palate with hints of cocoa. Impressive structure and length, with a soft and velvety finish.";
    String cofee = "A cappuccino is the perfect balance of espresso, steamed milk and foam. This coffee is all about the structure and the even splitting of all elements into equal thirds. An expertly made cappuccino should be rich, but not acidic and have a mildly sweet flavouring from the milk.";
    String beer = "A light, crisp beer, pilsners trace their history to the former Kingdom of Bohemia in the modern-day Czech Republic. Today, pilsners are available in Europe, North America, and worldwide. Traditionally pilsners are made with Bavarian bottom-fermenting lager yeasts, light malts, and floral, spicy Saaz hops.";
    String juice = "Orange juice is a favorite beverage high in antioxidants and micronutrients like vitamin C, folate, and potassium. Regular consumption has been associated with several health benefits, including improved heart health, decreased inflammation, and a reduced risk of kidney stones.";
    String wine = "Ferrari Perlé is made using the traditional metodo classico. After a hand harvest in September, the wine undergoes a gentle pressing and first fermentation in tanks. The wine is bottled and a second fermentation occurs with selected yeasts. At least 5 years on the yeasts, selected from our own strains.";
    NotificationBadge barge;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_product);
        db.insert(new Drink(images[0],"Clove Tea", "$10",90,CloveTeaDesc));
        db.insert(new Drink(images[1],"CockTail", "$2.50",10,cocktail));
        db.insert(new Drink(images[2],"masi brolo campofiorin oro 2018", "$11.99",100,Masi));
        db.insert(new Drink(images[3],"cocacola", "$3",30,cocacola));
        db.insert(new Drink(images[4],"Cappuccino", "$10",50,cofee));
        db.insert(new Drink(images[5],"Beer Pilsner", "$20",10,beer));
        db.insert(new Drink(images[6],"Orange Juice", "$3",190,juice));
        db.insert(new Drink(images[7],"Ferrari Perle Champagne", "$90",950,wine));

//        List<Drink> myListData = db.getAllDrinks().getValue();

       db.getAllDrinks().observe(this, new Observer<List<Drink>>() {
           @Override
           public void onChanged(List<Drink> models) {
               GridView gridView = (GridView) findViewById(R.id.recycler_view);
               ProductArrayAdapter adapter = new ProductArrayAdapter(getApplicationContext(),models,db);
               gridView.setAdapter(adapter);
           }
       });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_item, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        Intent intent;
        switch (item.getItemId())
        {
            case R.id.cart_item_id:
                 intent = new Intent(ProductActivity.this, CartActivity.class);
                startActivity(intent);
                return true;
            case R.id.admin_item_id:
                intent = new Intent(ProductActivity.this, AdminActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}