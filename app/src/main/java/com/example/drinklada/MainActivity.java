package com.example.drinklada;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.example.drinklada.models.ViewModal;
import com.nex3z.notificationbadge.NotificationBadge;

import java.util.Objects;

public class MainActivity extends AppCompatActivity {
    NotificationBadge barge;
    public static ViewModal db ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        db = new ViewModelProvider(this).get(ViewModal.class);
        Button continueBtn = findViewById(R.id.continue_btn);
        continueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ProductActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_item,menu);
        RecyclerView view  = (RecyclerView) menu.findItem(R.id.cart_item_id).getActionView();
        barge = view.findViewById(R.id.badge);
//        MenuItem item = menu.findItem(R.id.cart_item_id);
//        RelativeLayout relativeLayout = (RelativeLayout) MenuItemCompat.setActionView(item, R.layout.action_bar_notification_icon).getActionView();
//        barge = (NotificationBadge) relativeLayout.findViewById(R.id.badge);

        updateCartCount();
        return super.onCreateOptionsMenu(menu);
    }

    private void updateCartCount() {
        int count = Objects.requireNonNull(db.getAllCartDrinks().getValue()).size();
        if (barge ==null) return;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (count == 0){
                    barge.setVisibility(View.INVISIBLE);
                }else {
                    barge.setVisibility(View.VISIBLE);
                    barge.setText(String.valueOf(count));
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id==R.id.cart_item_id){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}