package com.example.drinklada.models;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "cart_table")
public class CartDrink implements Serializable {
    @PrimaryKey(autoGenerate = true)
    int id;
    String image;
    String name;
    String price;
    int quantity;

    public CartDrink(String image, String name, String price) {
        this.image = image;
        this.name = name;
        this.price = price;
        this.quantity = 0;
    }

    public CartDrink(int id, String image, String name, String price, int quantity) {
        this.id = id;
        this.image = image;
        this.name = name;
        this.price = price;
        this.quantity = quantity;
    }

    public CartDrink(String image, String name, String price, int quantity) {
        this.image = image;
        this.name = name;
        this.price = price;
        this.quantity = quantity;
    }

    public CartDrink() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

}
