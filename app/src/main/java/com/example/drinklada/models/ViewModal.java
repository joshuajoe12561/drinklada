package com.example.drinklada.models;


import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class ViewModal extends AndroidViewModel {

    // creating a new variable for course repository.
    private DrinkRepository repository;

    // below line is to create a variable for live
    // data where all the courses are present.
    private LiveData<List<Drink>> allDrinks;
    private LiveData<List<CartDrink>> allCartDrinks;

    // constructor for our view modal.
    public ViewModal(@NonNull Application application) {
        super(application);
        repository = new DrinkRepository(application);
        allDrinks = repository.getAllDrinks();
        allCartDrinks = repository.getAllCartDrinks();
    }

    public void insert(Drink model) {
        repository.insert(model);
    }
    public void insert(CartDrink model) {
        repository.insertCartDrink(model);
    }

    // below line is to update data in our repository.
    public void update(Drink model) {
        repository.update(model);
    }
    public void update(CartDrink model) {
        repository.update(model);
    }

    // below line is to delete the data in our repository.
    public void delete(Drink model) {
        repository.delete(model);
    }
    public void delete(CartDrink model) {
        repository.delete(model);
    }
    // below method is to delete all the courses in our list.
    public void deleteAllDrink() {
        repository.deleteAllDrinks();
    }
    public void deleteAllCartDrink() {
        repository.deleteAllCartDrink();
    }

    // below method is to get all the courses in our list.
    public LiveData<List<Drink>> getAllDrinks() {
        return allDrinks;
    }
    public LiveData<List<CartDrink>> getAllCartDrinks() {
        return allCartDrinks;
    }
}
