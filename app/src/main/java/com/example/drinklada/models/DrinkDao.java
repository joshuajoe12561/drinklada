package com.example.drinklada.models;

import androidx.lifecycle.LiveData;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@androidx.room.Dao
public interface DrinkDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Drink drink);
    @Update
    void update(Drink drink);

    @Delete
    void delete(Drink drink);

    @Query("DELETE FROM drink_table")
    void deleteAllDrink();

    @Query("SELECT * FROM drink_table ORDER BY name ASC")
    LiveData<List<Drink>> getAllDrink();
}
