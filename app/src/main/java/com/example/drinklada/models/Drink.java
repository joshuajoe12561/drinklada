package com.example.drinklada.models;

import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "drink_table", indices =@Index(value = {"name"}, unique = true))
public class Drink implements Serializable {
    @PrimaryKey(autoGenerate = true)
    int id;
    String image;
    String name;
    String price;
    int quantity;
    String description;

    public Drink(String image, String name, String price) {
        this.image = image;
        this.name = name;
        this.price = price;
        this.quantity = 0;
    }

    public Drink(int id, String image, String name, String price, int quantity, String description) {
        this.id = id;
        this.image = image;
        this.name = name;
        this.price = price;
        this.quantity = quantity;
        this.description = description;
    }

    public Drink(String image, String name, String price, int quantity, String description) {
        this.image = image;
        this.name = name;
        this.price = price;
        this.quantity = quantity;
        this.description = description;
    }

    public Drink() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
