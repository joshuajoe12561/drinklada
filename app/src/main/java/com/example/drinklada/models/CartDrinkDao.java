package com.example.drinklada.models;

import androidx.lifecycle.LiveData;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@androidx.room.Dao
public interface CartDrinkDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(CartDrink drink);

    @Update
    void update(CartDrink drink);

    @Delete
    void delete(CartDrink drink);

    @Query("DELETE FROM cart_table")
    void deleteAllCartDrink();

    @Query("SELECT * FROM cart_table ORDER BY name ASC")
    LiveData<List<CartDrink>> getAllCartDrinks();
}
