package com.example.drinklada.models;


import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.example.drinklada.db.DatabaseDrink;

import java.util.List;

public class DrinkRepository {

    // below line is the create a variable
    // for dao and list for all courses.
    private DrinkDao drinkDao;
    private CartDrinkDao cartDrinkDao;
    private LiveData<List<Drink>> allDrinks;
    private LiveData<List<CartDrink>> allCartDrinks;

    // creating a constructor for our variables
    // and passing the variables to it.
    public DrinkRepository(Application application) {
        DatabaseDrink database = DatabaseDrink.getInstance(application);
        drinkDao = database.drinkDao();
        cartDrinkDao = database.cartDrinkDao();
        allDrinks = drinkDao.getAllDrink();
        allCartDrinks = cartDrinkDao.getAllCartDrinks();
    }

    // creating a method to insert the data to our database.

    public void insert(Drink model) {
        new InsertDrinkAsyncTask(drinkDao).execute(model);
    }

    // creating a method to update data in database.
    public void update(Drink model) {
        new UpdateDrinkAsyncTask(drinkDao).execute(model);
    }

    // creating a method to delete the data in our database.
    public void delete(Drink model) {
        new DeleteDrinkAsyncTask(drinkDao).execute(model);
    }

    // below is the method to delete all the courses.
    public void deleteAllDrinks() {
        new DeleteAllDrinkAsyncTask(drinkDao).execute();
    }

    // below method is to read all the courses.
    public LiveData<List<Drink>> getAllDrinks() {
        return allDrinks;
    }

    public void insertCartDrink(CartDrink model) {
        new InsertCartDrinkAsyncTask(cartDrinkDao).execute(model);
    }
    public void update(CartDrink model) {
        new UpdateCartDrinkAsyncTask(cartDrinkDao).execute(model);
    }
    public void delete(CartDrink model) {
        new DeleteCartDrinkAsyncTask(cartDrinkDao).execute(model);
    }
    public void deleteAllCartDrink() {
        new DeleteAllCartDrinkAsyncTask(cartDrinkDao).execute();
    }
    public LiveData<List<CartDrink>> getAllCartDrinks() {
        return cartDrinkDao.getAllCartDrinks();
    }

    // we are creating a async task method to insert new course.
    private static class InsertDrinkAsyncTask extends AsyncTask<Drink, Void, Void> {
        private DrinkDao dao;

        private InsertDrinkAsyncTask(DrinkDao dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(Drink... model) {
            // below line is use to insert our modal in dao.
            dao.insert(model[0]);
            return null;
        }
    }

    // we are creating a async task method to update our course.
    private static class UpdateDrinkAsyncTask extends AsyncTask<Drink, Void, Void> {
        private DrinkDao dao;

        private UpdateDrinkAsyncTask(DrinkDao dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(Drink... models) {
            // below line is use to update
            // our modal in dao.
            dao.update(models[0]);
            return null;
        }
    }

    // we are creating a async task method to delete course.
    private static class DeleteDrinkAsyncTask extends AsyncTask<Drink, Void, Void> {
        private DrinkDao dao;

        private DeleteDrinkAsyncTask(DrinkDao dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(Drink... models) {
            // below line is use to delete
            // our course modal in dao.
            dao.delete(models[0]);
            return null;
        }
    }

    // we are creating a async task method to delete all courses.
    private static class DeleteAllDrinkAsyncTask extends AsyncTask<Void, Void, Void> {
        private DrinkDao dao;
        private DeleteAllDrinkAsyncTask(DrinkDao dao) {
            this.dao = dao;
        }
        @Override
        protected Void doInBackground(Void... voids) {
            // on below line calling method
            // to delete all courses.
            dao.deleteAllDrink();
            return null;
        }
    }
    private static class InsertCartDrinkAsyncTask extends AsyncTask<CartDrink, Void, Void> {
        private CartDrinkDao dao;

        private InsertCartDrinkAsyncTask(CartDrinkDao dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(CartDrink... model) {
            // below line is use to insert our modal in dao.
            dao.insert(model[0]);
            return null;
        }
    }

    // we are creating a async task method to update our course.
    private static class UpdateCartDrinkAsyncTask extends AsyncTask<CartDrink, Void, Void> {
        private CartDrinkDao dao;

        private UpdateCartDrinkAsyncTask(CartDrinkDao dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(CartDrink... models) {
            // below line is use to update
            // our modal in dao.
            dao.update(models[0]);
            return null;
        }
    }

    // we are creating a async task method to delete course.
    private static class DeleteCartDrinkAsyncTask extends AsyncTask<CartDrink, Void, Void> {
        private CartDrinkDao dao;

        private DeleteCartDrinkAsyncTask(CartDrinkDao dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(CartDrink... models) {
            // below line is use to delete
            // our course modal in dao.
            dao.delete(models[0]);
            return null;
        }
    }

    // we are creating a async task method to delete all courses.
    private static class DeleteAllCartDrinkAsyncTask extends AsyncTask<Void, Void, Void> {
        private CartDrinkDao dao;
        private DeleteAllCartDrinkAsyncTask(CartDrinkDao dao) {
            this.dao = dao;
        }
        @Override
        protected Void doInBackground(Void... voids) {
            // on below line calling method
            // to delete all courses.
            dao.deleteAllCartDrink();
            return null;
        }
    }
}
