package com.example.drinklada.db;


import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.example.drinklada.models.CartDrink;
import com.example.drinklada.models.CartDrinkDao;
import com.example.drinklada.models.Drink;
import com.example.drinklada.models.DrinkDao;

// adding annotation for our database entities and db version.
@Database(entities = {Drink.class, CartDrink.class}, version = 1)
public abstract class DatabaseDrink extends RoomDatabase {

    private static DatabaseDrink instance;

    public abstract DrinkDao drinkDao();
    public abstract CartDrinkDao cartDrinkDao();

    // on below line we are getting instance for our database.
    public static synchronized DatabaseDrink getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(),
                            DatabaseDrink.class, "drink_database")
                            .fallbackToDestructiveMigration()
                            .addCallback(roomCallback)
                            .build();
        }
        return instance;
    }

    // below line is to create a callback for our room database.
    private static RoomDatabase.Callback roomCallback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new PopulateDbAsyncTask(instance).execute();
        }
    };

    // we are creating an async task class to perform task in background.
    private static class PopulateDbAsyncTask extends AsyncTask<Void, Void, Void> {
        PopulateDbAsyncTask(DatabaseDrink instance) {
            DrinkDao drinkDao = instance.drinkDao();
            CartDrinkDao cartDrinkDao = instance.cartDrinkDao();
        }
        @Override
        protected Void doInBackground(Void... voids) {
            return null;
        }
    }
}
